<?php
/**
 * Edtech Vimeo Plugin
 *
 * PHP version 5
 *
 * @category WordPress
 * @package  Edtech_Vimeo_Plugin
 * @author   John Bicknell <john@geckotec.co.uk>
 * @license  unlicensed http://www.unlicensed.com
 * @version  GIT: 0.1.2
 * @link     http://www.geckotec.co.uk
 *
 * @wordpress-plugin
 * Plugin Name: Edtech Vimeo Plugin
 * Description: Sets up vimeo video player embed
 * Version: 0.1.2
 * Author: John Bicknell
 **/

// js script to dynamically resize iframe in responsive layout.
add_action(
	'wp_enqueue_scripts', function () {
		wp_enqueue_script( 'iframe-resizer', '//vimeo.ib-ed.tech/resize.js' );
	}
);

// js script to dynamically resize iframe in responsive layout but in edit mode.
add_action(
	'admin_enqueue_scripts', function () {
		wp_enqueue_script( 'iframe-resizer', '//vimeo.ib-ed.tech/resize.js', null, null, true );
	}
);

// add ed tech domain to white list of sites that can use oembed.
add_action(
	'init', function () {
		wp_oembed_add_provider(
			'http://vimeo.ib-ed.tech/player/*',
			'http://vimeo.ib-ed.tech/oembed'
		);
	}
);

